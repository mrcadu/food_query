package br.ufrj.food_query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodQueryApplication.class, args);
    }

}
