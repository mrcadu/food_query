package br.ufrj.food_query.repository;

import br.ufrj.food_query.model.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends JpaRepository<Food, Double> {
    List<Food> findAllByDescricaoContaining(String descricao);
}
