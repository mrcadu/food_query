package br.ufrj.food_query.controller;

import br.ufrj.food_query.model.Food;
import br.ufrj.food_query.repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/food")
public class FoodController {
    @Autowired
    private FoodRepository foodRepository;

    @CrossOrigin
    @GetMapping
    private List<Food> findFoodByDescription(@RequestParam String description){
        return foodRepository.findAllByDescricaoContaining(description);
    }
}
