package br.ufrj.food_query.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Double id;

    private Double codigo;
    private String descricao;
    private Double energia;
    private Double proteina;
    private Double lipidios;
    private Double carboidratos;
    private Double fibra;
    private Double colesterol;
    private Double saturados;
    private Double mono;
    private Double poli;
    private Double linoleico;
    private Double linolenico;
    private Double trans;
    private Double acucar;
    private Double acucaradd;
    private Double calcio;
    private Double magnesio;
    private Double manganes;
    private Double fosforo;
    private Double ferro;
    private Double sodio;
    private Double sodioadd;
    private Double potassio;
    private Double cobre;
    private Double zinco;
    private Double selenio;
    private Double retinol;
    private Double vitaminaa;
    private Double tiamina;
    private Double riboflavina;
    private Double niacina;
    private Double niacinane;
    private Double piridoxina;
    private Double cobalamina;
    private Double folato;
    private Double vitaminad;
    private Double vitaminae;
    private Double vitaminac;
}

